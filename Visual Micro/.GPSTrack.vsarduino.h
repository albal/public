#ifndef _VSARDUINO_H_
#define _VSARDUINO_H_
//Board = Arduino Mega 2560 or Mega ADK
#define __AVR_ATmega2560__
#define ARDUINO 105
#define ARDUINO_MAIN
#define __AVR__
#define F_CPU 16000000L
#define __cplusplus
#define __inline__
#define __asm__(x)
#define __extension__
#define __ATTR_PURE__
#define __ATTR_CONST__
#define __inline__
#define __asm__ 
#define __volatile__
#define __builtin_va_list
#define __builtin_va_start
#define __builtin_va_end
#define __DOXYGEN__
#define __attribute__(x)
#define NOINLINE __attribute__((noinline))
#define prog_void
#define PGM_VOID_P int

typedef unsigned char byte;
extern "C" void __cxa_pure_virtual() {;}

//
//
void SetupGPS(void);
void ReadGPSTime(void);
void ReadGPSPos(void);
void CreateData();
void SendData(void);
void rtty_txstring (char * string);
void rtty_txbyte (char c);
void rtty_txbit (int bit);
void sendUBX(uint8_t *MSG, uint8_t len);
boolean getUBX_ACK(uint8_t *MSG);
boolean checkNAV();
uint16_t gps_CRC16_checksum (char *string);
void gps_ubx_checksum(uint8_t* data, uint8_t len, uint8_t* cka, 					  uint8_t* ckb);
bool _gps_verify_checksum(uint8_t* data, uint8_t len);
void gps_get_data();
void gps_check_lock();
void gps_get_position();
void gps_get_time();
void setupGPS();
void gpsPower(int i);
void printhex(char b);
void printhex(uint8_t data []);

#include "C:\Program Files (x86)\Arduino\hardware\arduino\variants\mega\pins_arduino.h" 
#include "C:\Program Files (x86)\Arduino\hardware\arduino\cores\arduino\arduino.h"
#include "C:\Users\westa\Documents\Arduino\GPSTrack\GPSTrack.ino"
#endif
