/*  

Copywrong message

// https://github.com/jamescoxon/PicoAtlas/commit/fc7da5268e98f9494d4a7a3b8fe429eadd0899c4

*/ 

#include <string.h>
#include <util/crc16.h>
#include <SoftwareSerial.h>

SoftwareSerial GPS(11,12);
#define RADIOPIN 13

unsigned int CHECKSUM;
int32_t lat = 0, lon = 0, alt = 0;
uint8_t hour = 0, minute = 0, second = 0, lock = 0, sats = 0;
unsigned long startGPS = 0, startTime = 0, ts = 0; 
int GPSerror = 0, count = 1, n, gpsstatus, lockcount = 0, intTemp = 0, total_time = -1, radiostatus = 0, navmode = 9, volts = 0, data_av = 0, y;

uint8_t buf[60]; //GPS receive buffer

boolean stringLoaded = false, wanted = false, GPSGO = false;
byte gps_set_sucess = 0 ;
char datastring[80], ch = 0, checksum_str[6] = "00000";
int min = 32000, max = 0, i=0, len=0;
unsigned long samples = 0, average = 0;

// match char array to char consts - i.e match (myarray, "My String", 9);
boolean myMatch(char a[],char b[],int array_size = 6)
{
	for (int k = 0; k < array_size; ++k)
		if (a[k] != b[k])
			return(false);
	return(true);
}

/**********************************************************/

void setup() {                
	// Set LED13 OUTPUT (RADIOPIN)
	pinMode(RADIOPIN,OUTPUT);
	digitalWrite(RADIOPIN, HIGH);
	// Setup comms
	GPS.begin(9600);
	Serial.begin(9600);
	Serial.flush();
	Serial.println("COMM SETUP");
	Serial.println("WAIT FOR GPS");

	delay(2000);

	// GPS to communicate at 4800bps
	GPS.print("$PUBX,41,1,0007,0003,4800,0*13\r\n"); 
	GPS.begin(4800);
	GPS.flush();
	Serial.println("GPS now at 4800bps");
	digitalWrite(RADIOPIN, LOW);
}

void loop()
{
	setupGPS();

	while (true)		// MAIN LOOP
	{
		ReadGPSTime();
		ReadGPSPos();
		gps_check_lock();
		delay(200);
		CreateData();
		delay(200);
		SendData();
		delay(200);
	}

}

void SetupGPS(void)
{
	// Setup GPS Flight mode
	Serial.println("Setting uBlox nav mode: ");
	uint8_t setNav[] = {0xB5, 0x62, 0x06, 0x24, 0x24, 0x00, 0xFF, 0xFF, 0x06, 0x03, 0x00, 0x00, 0x00, 0x00, 0x10, 0x27, 0x00, 0x00, 0x05, 0x00, 0xFA, 0x00, 0xFA, 0x00, 0x64, 0x00, 0x2C, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x16, 0xDC};
	while(!gps_set_sucess)
	{
		sendUBX(setNav, sizeof(setNav)/sizeof(uint8_t));
		gps_set_sucess=getUBX_ACK(setNav);
	}
	gps_set_sucess=0;
}

void ReadGPSTime(void)
{
	ts = millis();
	Serial.print("----  Reading GPS Time:");
	gps_get_time();



	/*
	// Read GPS Strings
	Serial.print(">>>>> Read: ");
	while ( GPS.available() && !stringLoaded)
	{
	ch = GPS.read();
	Serial.write(ch);
	switch (ch)
	{
	case '$':
	i = 0;
	gpstring[i] = ch;
	break;

	case '*':
	// read two more characters for checksum
	gpstring[i++] = '*';
	gpstring[i++] = GPS.read();
	gpstring[i++] = GPS.read();
	gpstring[i++] = '\0';		// Add a string terminator
	stringLoaded = true;		// indicate we are loaded
	break;

	default:
	if (gpstring[0] == '$')		// if we are loading a string
	gpstring[i] = ch;		// then add in new character
	} // End of case


	if (i < 100)					// if we are still under 100 chars for a line
	i++;						// or else we have a problem
	else
	{
	i=0;
	gpstring[0] = 'E';
	gpstring[1] = '\0';
	Serial.println("EEEEE Roll over EEEEE");
	}

	} // End of while

	// show the full string on serial
	Serial.print(" ====== ");
	Serial.print(gpstring);

	// Calc how long we were in the loop
	Serial.print(" ||||| timing: ");
	Serial.print(millis() - ts);
	Serial.print("ms");

	ts = millis();

	// change string sent depending on message
	//		if (myMatch(gpstring, "$GPGGA"))
	//		{
	//			Serial.println("  WANTED");
	//			wanted = true;
	//		}
	//		else 
	if (myMatch(gpstring, "$GPRMC"))
	{
	Serial.println("  WANTED");
	wanted = true;
	}
	else
	Serial.println("  DISCARDED");

	*/
}

void ReadGPSPos(void) {

	count++;
	Serial.print("----  Reading GPS Position:");
	gps_get_position();

}


void CreateData()
{

	// $$ABC001,123,12:34:56, 51.93332,-1.382738,32.001,14.6*2F23
	sprintf (datastring, "$$ABC001,%d,%02d:%02d:%02d,%ld,%ld,%ld,%d", count, hour, minute, second, lat, lon, alt, sats);
	//sprintf (datastring, "$$ABC001,%02d%02d%02d,%ld,%ld,%ld", hour, minute, second, lat, lon, alt);

	CHECKSUM = gps_CRC16_checksum(datastring);  // Calculates the checksum for this datastring
	sprintf(checksum_str, "*%04X\n", CHECKSUM);

	Serial.print("<<<<< ");
	Serial.print(datastring);
	Serial.print(checksum_str);
}

void SendData(void)
{
	strcat(datastring,checksum_str);
	rtty_txstring (datastring);
}


void rtty_txstring (char * string)
{
	// To disable interrupts:
	cli(); // disable global interrupts
	delayMicroseconds(10000);

	/* Simple function to sent a char at a time to 
	** rtty_txbyte function. 
	** NB Each char is one byte (8 Bits)
	*/
	char c;
	c = *string++;
	while ( c != '\0')
	{
		rtty_txbyte (c);
		c = *string++;
	}
	// and to enable them: 
	delayMicroseconds(10000);
	sei(); // enable interrupts
}


void rtty_txbyte (char c)
{
	/* Simple function to sent each bit of a char to 
	** rtty_txbit function. 
	** NB The bits are sent Least Significant Bit first
	**
	** All chars should be preceded with a 0 and 
	** proceded with a 1. 0 = Start bit; 1 = Stop bit
	**
	*/

	int i;
	rtty_txbit (0); // Start bit

	// Send bits for for char LSB first	
	for (i=0;i<7;i++) // Change this here 7 or 8 for ASCII-7 / ASCII-8
	{
		if (c & 1) 
			rtty_txbit(1); 
		else 
			rtty_txbit(0);	
		c = c >> 1;
	}

	rtty_txbit (1); // Stop bit
	rtty_txbit (1); // Stop bit
}

void rtty_txbit (int bit)
{
	if (bit)
		// high
			digitalWrite(RADIOPIN, HIGH);
	else
		// low
		digitalWrite(RADIOPIN, LOW);

	//elayMicroseconds(3320); // 300 baud  (3370)
	delayMicroseconds(10000); // For 50 Baud uncomment this and the line below. 
	delayMicroseconds(10150); // You can't do 20150 it just doesn't work as the
	// largest value that will produce an accurate delay is 16383
	// See : http://arduino.cc/en/Reference/DelayMicroseconds

}


// Send a byte array of UBX protocol to the GPS
void sendUBX(uint8_t *MSG, uint8_t len) {
	for(int i=0; i<len; i++)
		GPS.write(MSG[i]);
	GPS.println();
}

// Calculate expected UBX ACK packet and parse UBX response from GPS
boolean getUBX_ACK(uint8_t *MSG) {
	uint8_t b, ackByteID = 0, ackPacket[10];
	unsigned long startTime = millis();
	// Construct the expected ACK packet    
	ackPacket[0] = 0xB5;	// header
	ackPacket[1] = 0x62;	// header
	ackPacket[2] = 0x05;	// class
	ackPacket[3] = 0x01;	// id
	ackPacket[4] = 0x02;	// length
	ackPacket[5] = 0x00;
	ackPacket[6] = MSG[2];	// ACK class
	ackPacket[7] = MSG[3];	// ACK id
	ackPacket[8] = 0;		// CK_A
	ackPacket[9] = 0;		// CK_B

	// Calculate the checksums
	for (uint8_t i=2; i<8; i++) {
		ackPacket[8] = ackPacket[8] + ackPacket[i];
		ackPacket[9] = ackPacket[9] + ackPacket[8];
	}

	while (true) {

		// Test for success
		if (ackByteID > 9) {
			// All packets in order!
			Serial.println("Success");
			return true;
		}

		// Timeout if no valid response in 3 seconds
		if (millis() - startTime > 3000) { 
			Serial.println("Fail");
			return false;
		}

		// Make sure data is available to read
		if (GPS.available()) {
			b = GPS.read();
			// Check that bytes arrive in sequence as per expected ACK packet
			if (b == ackPacket[ackByteID]) { 
				ackByteID++;
				//Serial.print(b, HEX);
			} 
			else 
				ackByteID = 0;	// Reset and look again, invalid order
		}
	}
}


//************Other Functions*****************

//Function to poll the NAV5 status of a Ublox GPS module (5/6)
//Sends a UBX command (requires the function sendUBX()) and waits 3 seconds
// for a reply from the module. It then isolates the byte which contains 
// the information regarding the NAV5 mode,
// 0 = Pedestrian mode (default, will not work above 12km)
// 6 = Airborne 1G (works up to 50km altitude)
//Adapted by jcoxon from getUBX_ACK() from the example code on UKHAS wiki
// http://wiki.ukhas.org.uk/guides:falcom_fsa03
boolean checkNAV(){
	uint8_t b, bytePos = 0;
	uint8_t getNAV5[] = { 0xB5, 0x62, 0x06, 0x24, 0x00, 0x00, 0x2A, 0x84 }; //Poll NAV5 status

	GPS.flush();
	unsigned long startTime = millis();
	sendUBX(getNAV5, sizeof(getNAV5)/sizeof(uint8_t));

	while (1) {
		// Make sure data is available to read
		if (GPS.available()) {
			b = GPS.read();

			if(bytePos == 8){
				navmode = b;
				return true;
			}

			bytePos++;
		}
		// Timeout if no valid response in 3 seconds
		if (millis() - startTime > 3000) {
			navmode = 0;
			return false;
		}
	}
}


uint16_t gps_CRC16_checksum (char *string)
{
	size_t i;
	uint16_t crc;
	uint8_t c;

	crc = 0xFFFF;

	// Calculate checksum ignoring the first two $s
	for (i = 2; i < strlen(string); i++)
	{
		c = string[i];
		crc = _crc_xmodem_update (crc, c);
	}

	return crc;
}

/**
* Calculate a UBX checksum using 8-bit Fletcher (RFC1145)
*/
void gps_ubx_checksum(uint8_t* data, uint8_t len, uint8_t* cka,
					  uint8_t* ckb)
{
	*cka = 0;
	*ckb = 0;
	for( uint8_t i = 0; i < len; i++ )
	{
		*cka += *data;
		*ckb += *cka;
		data++;
	}
}

/**
* Verify the checksum for the given data and length.
*/
bool _gps_verify_checksum(uint8_t* data, uint8_t len)
{
	uint8_t a, b;
	gps_ubx_checksum(data, len, &a, &b);
	if( a != *(data + len) || b != *(data + len + 1))
		return false;
	else
		return true;
}

/**
* Get data from GPS, times out after 1 second.
*/
void gps_get_data()
{
	int i = 0;
	unsigned long startTime = millis();
	while (1) {
		// Make sure data is available to read
		if (GPS.available()) {
			buf[i] = GPS.read();
			i++;
		}
		// Timeout if no valid response in 1 seconds
		if (millis() - startTime > 1000) {
			break;
		}
	}
}
/**
* Check the navigation status to determine the quality of the
* fix currently held by the receiver with a NAV-STATUS message.
*/
void gps_check_lock()
{
	GPSerror = 0;
	GPS.flush();
	// Construct the request to the GPS
	uint8_t request[8] = {0xB5, 0x62, 0x01, 0x06, 0x00, 0x00,
		0x07, 0x16};
	sendUBX(request, 8);

	// Get the message back from the GPS
	gps_get_data();
	// Verify the sync and header bits
	if( buf[0] != 0xB5 || buf[1] != 0x62 ) {
		GPSerror = 11;
	}
	if( buf[2] != 0x01 || buf[3] != 0x06 ) {
		GPSerror = 12;
	}

	// Check 60 bytes minus SYNC and CHECKSUM (4 bytes)
	if( !_gps_verify_checksum(&buf[2], 56) ) {
		GPSerror = 13;
	}

	if(GPSerror == 0){
		// Return the value if GPSfixOK is set in 'flags'
		if( buf[17] & 0x01 )
			lock = buf[16];
		else
			lock = 0;

		sats = buf[53];
	}
	else {
		lock = 0;
	}
}

/**
* Poll the GPS for a position message then extract the useful
* information from it - POSLLH.
*/
void gps_get_position()
{
	GPSerror = 0;
	GPS.flush();
	// Request a NAV-POSLLH message from the GPS
	uint8_t request[8] = {0xB5, 0x62, 0x01, 0x02, 0x00, 0x00, 0x03,
		0x0A};
	sendUBX(request, 8);

	// Get the message back from the GPS
	gps_get_data();

	// Verify the sync and header bits
	if( buf[0] != 0xB5 || buf[1] != 0x62 )
		GPSerror = 21;
	if( buf[2] != 0x01 || buf[3] != 0x02 )
		GPSerror = 22;

	if( !_gps_verify_checksum(&buf[2], 32) ) {
		GPSerror = 23;
	}

	if(GPSerror == 0) {
		// 4 bytes of longitude (1e-7)
		lon = (int32_t)buf[10] | (int32_t)buf[11] << 8 | 
			(int32_t)buf[12] << 16 | (int32_t)buf[13] << 24;
		//lon /= 1000;

		// 4 bytes of latitude (1e-7)
		lat = (int32_t)buf[14] | (int32_t)buf[15] << 8 | 
			(int32_t)buf[16] << 16 | (int32_t)buf[17] << 24;
		//lat /= 1000;

		// 4 bytes of altitude above MSL (mm)
		alt = (int32_t)buf[22] | (int32_t)buf[23] << 8 | 
			(int32_t)buf[24] << 16 | (int32_t)buf[25] << 24;
		alt /= 1000;
	}

}

/**
* Get the hour, minute and second from the GPS using the NAV-TIMEUTC
* message.
*/
void gps_get_time()
{
	GPSerror = 0;
	GPS.flush();
	// Send a NAV-TIMEUTC message to the receiver
	uint8_t request[8] = {0xB5, 0x62, 0x01, 0x21, 0x00, 0x00,
		0x22, 0x67};
	sendUBX(request, 8);

	// Get the message back from the GPS
	gps_get_data();

	// Verify the sync and header bits
	if( buf[0] != 0xB5 || buf[1] != 0x62 )
		GPSerror = 31;
	if( buf[2] != 0x01 || buf[3] != 0x21 )
		GPSerror = 32;

	if( !_gps_verify_checksum(&buf[2], 24) ) {
		GPSerror = 33;
	}

	if(GPSerror == 0) {
		if(hour > 23 || minute > 59 || second > 59)
		{
			GPSerror = 34;
		}
		else {
			hour = buf[22];
			minute = buf[23];
			second = buf[24];
			Serial.print("Got New Time");

		}
	}
}

void setupGPS() {
	//Turning off all GPS NMEA strings apart on the uBlox module

	//disable all default NMEA messages
	uint8_t noNMEA1[] ={0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x05,0x00, 0xFF, 0x19};
	uint8_t noNMEA2[] ={0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x03, 0x00, 0xFD, 0x15};
	uint8_t noNMEA3[] ={0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x01, 0x00, 0xFB, 0x11};
	uint8_t noNMEA4[] ={0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x00, 0x00, 0xFA, 0x0F};
	uint8_t noNMEA5[] ={0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x02, 0x00, 0xFC, 0x13};
	uint8_t noNMEA6[] ={0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x04, 0x00, 0xFE, 0x17}; 

	// Set Nav mode 1g
	uint8_t setNav[] = {
		0xB5, 0x62, 0x06, 0x24, 0x24, 0x00, 0xFF, 0xFF, 
		0x06, 0x03, 0x00, 0x00, 0x00, 0x00, 0x10, 0x27, 
		0x00, 0x00, 0x05, 0x00, 0xFA, 0x00, 0xFA, 0x00, 
		0x64, 0x00, 0x2C, 0x01, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x16, 0xDC};

	gps_set_sucess = 0;
	while(!gps_set_sucess)
	{
		sendUBX(noNMEA1, sizeof(noNMEA1)/sizeof(uint8_t));
		gps_set_sucess = getUBX_ACK(noNMEA1);
	}
	gps_set_sucess = 0;
	while(!gps_set_sucess)
	{
		sendUBX(noNMEA2, sizeof(noNMEA2)/sizeof(uint8_t));
		gps_set_sucess = getUBX_ACK(noNMEA2);
	}
	gps_set_sucess = 0;
	while(!gps_set_sucess)
	{
		sendUBX(noNMEA3, sizeof(noNMEA3)/sizeof(uint8_t));
		gps_set_sucess = getUBX_ACK(noNMEA3);
	}
	gps_set_sucess = 0;
	while(!gps_set_sucess)
	{
		sendUBX(noNMEA4, sizeof(noNMEA4)/sizeof(uint8_t));
		gps_set_sucess = getUBX_ACK(noNMEA4);
	}
	gps_set_sucess = 0;
	while(!gps_set_sucess)
	{
		sendUBX(noNMEA5, sizeof(noNMEA5)/sizeof(uint8_t));
		gps_set_sucess = getUBX_ACK(noNMEA5);
	}
	gps_set_sucess = 0;
	while(!gps_set_sucess)
	{
		sendUBX(noNMEA6, sizeof(noNMEA6)/sizeof(uint8_t));
		gps_set_sucess = getUBX_ACK(noNMEA6);
	}
	gps_set_sucess = 0;
	while(!gps_set_sucess)
	{
		sendUBX(setNav, sizeof(setNav)/sizeof(uint8_t));
		gps_set_sucess = getUBX_ACK(setNav);
	}
	gps_set_sucess = 0;
}

void gpsPower(int i){
	if(i == 0){
		//turn off GPS
		//  uint8_t GPSoff[] = {0xB5, 0x62, 0x02, 0x41, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x4D, 0x3B};
		uint8_t GPSoff[] = {0xB5, 0x62, 0x06, 0x04, 0x04, 0x00, 0x00, 0x00, 0x08, 0x00, 0x16, 0x74};
		sendUBX(GPSoff, sizeof(GPSoff)/sizeof(uint8_t));
		gpsstatus = 0;
	}
	else if (i == 1){
		//turn on GPS
		//uint8_t GPSon[] = {0xB5, 0x62, 0x02, 0x41, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x4C, 0x37};
		uint8_t GPSon[] = {0xB5, 0x62, 0x06, 0x04, 0x04, 0x00, 0x00, 0x00, 0x09, 0x00, 0x17, 0x76};
		sendUBX(GPSon, sizeof(GPSon)/sizeof(uint8_t));
		gpsstatus = 1;
		delay(1000);
		setupGPS();
	}
}

void printhex(char b)
{
	char up[5];
	sprintf(up, "%X", b);
	Serial.print(up);
}

void printhex(uint8_t data [])
{
	int len = sizeof(data);

	char up[5];
	for (i=0; i <= len; i++)
	{
		sprintf(up, "%X", data[i]);
		Serial.print(up);
		if ( i < len)
			Serial.print(",");
	}
}


